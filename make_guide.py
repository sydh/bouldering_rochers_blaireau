# coding: utf-8

import argparse
import re
import tempfile
import unicodedata
import urllib.parse
import urllib.request
from pathlib import Path

import bs4

base_url = "https://rochersdublaireau.wordpress.com"
area_choices = {
    "all": "",
    "main": ("circuit-bleu", "circuit-rouge", "circuit-noir", "hors-circuits"),
    "the cure": "liste-des-blocs-secteur-the-cure/",
    "la fontaine": "liste-des-blocs-secteur-la-fontaine/",
}

url_skip = (
    re.compile(r"(/contact-et-liens/)$"),
    re.compile(r"/\?share=(\w+)"),
    re.compile(r"(\.pdf)$")
)


def extract_routes(args, area_routes):

    tmp_folder = tempfile.mkdtemp()
    route_extracted_count = 0

    # TODO: use asyncio to scrap pages in //
    for area, route_info in area_routes.items():
        for route_data in route_info:

            print("Processing route: %s @ %s" % (route_data["title"], route_data["url"]))
            url = route_data["url"]
            with urllib.request.urlopen(url) as f:
                page_content = f.read().decode('utf-8')
                soup = bs4.BeautifulSoup(page_content, 'html.parser')

                title_entry = soup.find("h1", attrs={
                    "class": "title-header"
                })
                title = title_entry.text
                rgx = re.search(r"(?P<name>[\w\s]+)\s\((?P<grade>[\w+]+)\)", title)
                route_name: str = ""
                grade: str = ""
                if rgx:
                    name = rgx.groupdict()["name"]
                    grade = rgx.groupdict()["grade"]
                else:
                    # TODO: fix regex with strange character
                    print("Cannot extract grade from:", title)
                    name = route_name
                    # name = ""
                    grade = ""

                content_entry = soup.find("div", attrs={
                    "class": "entry-content clearfix"
                })
                if not content_entry:
                    raise RuntimeError("Unable to locate 'entry-content' for route: '%s', please report!", url)

                if area == "main":
                    entry_data = content_entry.find_all("a")

                    url_img = entry_data[0].get("href")
                    path_img = Path(tmp_folder) / Path(url_img).name
                    res = urllib.request.urlretrieve(url_img, path_img)
                    route_data["img"] = path_img

                    entry_data2 = content_entry.find_all("p")
                    # Note: fix for route: bleu 7 where description come with special characters in links
                    # it seems the html is not really well formatted here

                    entry_data2_desc = [e for e in entry_data2 if "Description" in e.text]
                    entry_data2_num = [e for e in entry_data2 if "Passage" in e.text]

                    if not entry_data2_desc:
                        raise RuntimeError("Could not find description")

                    # route_data["description"] = entry_data2[2].text.split(".", 1)[0]
                    route_data["description"] = entry_data2_desc[0].text
                    route_data["num"] = entry_data2_num[0].text if entry_data2_num else ""
                    route_data["name"] = name
                    route_data["grade"] = grade
                    route_data["name_grade"] = title

                else:
                    entry_data = content_entry.find_all("p")

                    url_img = entry_data[0].a.get("href")
                    path_img = Path(tmp_folder) / Path(url_img).name
                    res = urllib.request.urlretrieve(url_img, path_img)

                    route_data["description"] = entry_data[0].text
                    route_data["img"] = path_img

    # print("===")
    # print(area_routes)
    # print("===")
    return route_extracted_count, tmp_folder


def parse_areas(args):

    url = ""
    if args.area == "all":
        raise NotImplementedError
    else:

        area_url = area_choices[args.area]
        if not isinstance(area_url, str):
            if args.circuit:
                area_url = area_url[area_url.index(args.circuit)]
            else:
                area_url = area_url[0]
                print("No circuit specified, using", area_url)
        url = urllib.parse.urljoin(base_url, area_url)
        print("Extracting routes from: ", url)

    # TODO: add typing
    area_routes = {
    }

    with urllib.request.urlopen(url) as f:
        page_content = f.read().decode('utf-8')
        soup = bs4.BeautifulSoup(page_content, 'html.parser')

        content_entry = soup.find("div", attrs={
            "class": "entry-content clearfix"
        })
        if not content_entry:
            raise RuntimeError("Unable to locate 'entry-content', nothing to export")

        entry_links = content_entry.find_all("a")

        # Keep only url about routes
        skip_func = lambda url: any(rgx.search(url.get("href")) for rgx in url_skip)
        entry_links = [l for l in content_entry.find_all("a") if l.get("href") and not skip_func(l)]

        # FIXME: decouple from BS4?
        # FIXME: do not truncate entries
        entry_links = entry_links[:args.limit] if args.limit else entry_links
        area_routes[args.area] = [
            {"url": l.get("href"), "title": l.get("title")} for l in entry_links
        ]

    # print(area_routes)
    # print("===")
    extract_routes(args, area_routes)
    return area_routes


def make_pdf(area, routes, pdf_filename):

    import bookmark
    from image_util import get_image_size

    pdf = bookmark.Bookmark()

    # First page
    pdf.add_page()
    pdf.set_font("Arial", "B", 16.0)

    # Page header
    pdf.cell(4.0, 1.0, f"Secteur: {area}")
    pdf.ln(0.25)

    pdf.add_page()
    pdf.set_font('Arial', '', 10.0)

    for i, route in enumerate(routes, start=1):

        image = str(route["img"])
        title = route["title"]
        name = route["name"]
        width, height = get_image_size(image)
        ratio = float(width) / float(height)
        #print("w, h", width, height, ratio)
        # pdf.image(image, w=pdf.w / 2.0, h=pdf.h / 4.0)
        # TODO: use ratio for image height
        pdf.image(image, w=pdf.w, h=pdf.h * 0.75)
        pdf.ln(1.95)

        # Image caption
        pdf.cell(3.0, 0.0, "")
        pdf.ln(0.25)
        pdf.cell(3.0, 0.0, name)
        pdf.cell(3.0, 0.0, "")
        pdf.ln(1.25)
        pdf.ln(1.95)
        #print(route["description"])
        #print(route["description"].encode("latin-1", "ignore"))
        desc = unicodedata.normalize('NFKD', route["description"])
        #print(desc)
        #print(desc.encode("latin-1", error="replace"))
        desc2 = unicodedata.normalize('NFKD', route["description"]).encode('ascii', 'ignore')
        desc3 = desc2.decode("utf-8")
        pdf.cell(3.0, 0.0, desc3)
        pdf.ln(0.25)

        pdf.cell(3.0, 0.0, "")
        pdf.ln(1.25)
        pdf.ln(1.95)
        pdf.cell(3.0, 0.0, "Cotation: " + route["grade"])

        # name_grade = unicodedata.normalize('NFKD', route["name_grade"])
        name_grade = f"{i} " + route["name_grade"]
        name_grade2 = unicodedata.normalize('NFKD', name_grade).encode('ascii', 'ignore')
        name_grade3 = name_grade2.decode("utf-8")
        pdf.bookmark(txt=name_grade3)
        pdf.add_page()

    # output content into a file ('F')
    pdf.output(pdf_filename, 'F')


def make_guide_book(args):

    area_routes = parse_areas(args)
    for area, routes in area_routes.items():
        pdf_basename_no_ext = f"{area}"
        if args.circuit:
            pdf_basename_no_ext += f"-{args.circuit}"
        pdf_basename = pdf_basename_no_ext + ".pdf"
        make_pdf(area, routes, pdf_basename)
        print(f"Generated pdf: {pdf_basename}")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Rock bouldering guide books for `Rocher du Blaireau`")
    parser.add_argument("-a", "--area",
                        type=str,
                        choices=area_choices.keys(),
                        help="area to add in guide book")
    # TODO: support --all ?
    parser.add_argument("-c", "--circuit",
                        type=str,
                        choices=area_choices["main"],
                        help="select circuit (only for area: main)")

    parser.add_argument("-l", "--limit",
                        type=int,
                        help="only extract up to number of given routes")

    args = parser.parse_args()
    make_guide_book(args)
