###### Description

Scrap data from web site: https://rochersdublaireau.wordpress.com and generate pdf

###### Setup

* python3 -m venv venv
* venv/bin/python -m pip install -r requirements.txt

###### Usage
 
* venv/bin/python --area main -c circuit-bleu

###### License

see copying

